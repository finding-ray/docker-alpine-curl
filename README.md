# Alpine Linux Docker Image with Curl

A docker image built on alpine with curl installed. Very small and useful for making GitLab API calls from within GitLab CI.

&nbsp;
## Size

* 2.57 MB
* 2 layers
